# Cabify

## Cabify Excerice 1:  TeLlevo

This project consist in a simple data analyze about the company TeLlevo.
The original data set called **' Cabify_2010'** have information about:
- Drivers.
- User.
- Vehicles.
- Price and cost.
- Location points.
- Duration.

The main goal here it's:
1.  Process all the information.
2. Analyze all the data that we have.
4. Load all the processed data in a PostgreSql database.
5. Make queries to get detailed information.
6. Create a Dashboard with Tableau.

### [Process all the information](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/processing_data.ipynb)

In this part I am going to make the most typical checks:
- Check data type.
- Check the size of the dataframe.
- Check the unique values to see if i have some binary data or some column that contains only one differents value.

When we see that our dataframe have a lot null values we can delete the rows with null values or directly delete the columns that contains more than 25% of null values.

¿Why? Because the null values don't give us information. 

Otherwise, sometimes we can replace the null values by the mean of this column or the most typical value. But it's not the correct way to work in my opinion. 

So, in these case   we have deleted some columns that contained too many null values. 

At the end we have created a new .csv file called **'Cabify_clean_dataset.csv'** .

#### Location

In this project i wanted to use the latitude and longitud of each activity to get a summary with the postcode, region, city, country of each geo point, just to get more clearly and visual information. 

So, using the library geopy, i have got a .csv file called **'Location_cabify'** with all the information for some of the geo points that i have in the original data set of TeLlevo.

### [Analyze data](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/analyze_data.ipynb)

This part consist in:
- Create some histograms of the 'Cabify_clean_dataset' to see the range of some of the values and make some comments.
- Analyze the null values of the data set 'Location_cabify'.
- Process the information of the data set 'Location_cabify'.
- Make the merge between both data set to create only the main data set to save all the data in a database.

In this case we don't delete the null value of the 'Location_cabify' because is not part of the main information, it's just additional.

To make the merge of both data sets we use the 'latitude' and 'longitude' data.

At the end we create a new .csv file called **'Cabify_location_dataset'** .

### [Load all the processed data in a PostgreSql database](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/Load_database.ipynb)

In this part we load all the data in a Database that i have in AWS.
The process it's very simple:
- Create the connection with my database.
- Read the data set **'Cabify_location_dataset.csv'**.
- Save all the data in my Database.

All the information of the create table are [here](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/Database/Queries_exercice_1.sql).

### [Make queries to get detailed information](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/Results.ipynb)

Here i'm going to get information and answer some questions:
- Total differents drivers.
- Total differents  vechicles.
- Total differents  users.
- Most active users.
- Most active vehicles.
- Month with more activity.
- Best drivers of the months with more activity.
- Best origins.
- Month with more revenue.
- Check the revenue, cost, price, activity by month.
- Check the start_type.
- Price by Start type.

All the answers to these questions are [here](http://localhost:8889/notebooks/Results.ipynb#Check-the-month-with-more-revenue).

All the queries used are [here](https://gitlab.com/JorgeSola1/cabify-data-analyze/-/blob/master/Database/Queries_exercice_1.sql).

