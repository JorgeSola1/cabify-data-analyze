
-- Check the drivers that have worked in the month with more activity
select 
c.driver_id, 
count(c.driver_id) activity,
extract (month from start_at) as month
from cabify c 
where extract (month from start_at) = 12
group by month, c.driver_id
order by activity desc



--Check the month with more activity
select 
count(c.journey_id) as acitvity,
extract (month from start_at) as month
from cabify c 
group by month
order by acitvity desc


-- Check best origins
select 
count(c.journey_id) activity,
latitude ,
longitude 
from cabify c
group by c.latitude ,c.longitude
order by activity desc


--Check the month with more benefits
select 
avg(c.price) as price,
avg(c."cost") as cost,
(avg(c.price) - avg(c."cost")) as difference,
count(c.journey_id) as activity,
extract (month from start_at) as month
from cabify c 
group by month
order by difference desc

-- Check the count of the differents icons
select 
c.icon,
count(c.icon) as total_icon
from cabify c 
group by c.icon
order by total_icon desc


-- Check the taxis with more activities
select 
c.taxi_id,
count(c.taxi_id) as total_activity 
from cabify c 
group by c.taxi_id 
order by total_activity desc 

-- Check the activity of user
select 
user_id, 
count(user_id) total_activity
from public.cabify
group by user_id 
order by total_activity desc

-- Check the activity by start_type
select 
c.start_type,
count(c.start_type) total_start_type
from cabify c
group by c.start_type 
order by total_start_type desc

-- Check price by start_type
select 
c.start_type,
round(cast(avg(c.price) as numeric), 1) as price
from cabify c
group by c.start_type 
order by price desc