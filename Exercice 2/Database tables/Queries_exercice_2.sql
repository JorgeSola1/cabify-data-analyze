--Create tables

create table sales(
	sale_id text not null primary key,
	client_id text not null,
	tax_code text,
	currency text,
	amount int,
	notes text,
	create_at timestamp
);

create table sales_entries(
	sale_entry_id text not null primary key,
	sale_id text,
	price int,
	discount int,
	journey_id text,
	CONSTRAINT sales_id_reference FOREIGN KEY(sale_id) REFERENCES sales(sale_id),
    CONSTRAINT journey_id UNIQUE(journey_id)
);

create table journeys(
	journey_id text not null  primary key,
	country text,
	city text,
	FOREIGN KEY(journey_id) REFERENCES sales_entries(journey_id)
);

--Inserts:

insert into sales values ('a001','0001','npt','euro',200,'Medium service','2010-11-17 12:00:40');
insert into sales values ('a002','0001','apt','euro',500,'Good service','2010-11-16 17:29:57');
insert into sales values ('a003','0002','apt','euro',400,'Good service','2010-9-16 17:29:57');
insert into sales values ('a004','0003','npt','euro',800,'Good service','2010-9-17 12:15:27');

insert into sales_entries values('sale_001','a001',35,5,'j_0001');
insert into sales_entries values('sale_002','a001',60,10,'j_0002');

insert into journeys values ('j_0001','Spain','Zaragoza');
insert into journeys values ('j_0002','Spain','Madrid');


-- Queries

-- The total sale amount per month, year, and currency. That amount should be divided by 100 and truncated to one decimal.

select 
round((sum(s.amount)/100) ,1) as total_amount,
extract (month from s.create_at) as month
from sales s 
group by month 


-- The total discount per month, year and currency. Excluding data from 2017.
select 
sum(se.discount) as total_discount,
extract (month from s.create_at) as month,
extract (year from s.create_at) as year
from sales_entries se 
inner join sales s on s.sale_id = se.sale_id 
where extract (year from s.crate_at) != 2017
group by s.currency , month , year 


-- The total sale amount per city and country. Excluding those cities whose sales are lower than 1500.
select 
count(s.sale_id) as total_sales,
sum(s.amount) as total_amount,
j.city,
j.country 
from sales s 
inner join sales_entries se on se.sale_id = s.sale_id
inner join journeys j on j.journey_id = se.journey_id
where ((select 
		count(s2.sale_id) as total_sales
		from sales s2 
		inner join sales_entries se2 on se2.sale_id = s2.sale_id
		inner join journeys j2 on j2.journey_id = se2.journey_id
		where j2.city = j.city and j2.country = j.country 
		group by j.city , j.country) < 1500)
group by j.city , j.country 